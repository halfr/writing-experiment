#! /usr/bin/env python
# -*- encoding: utf8 -*-
#       lwt.py Comprensive use of Git
#
#       Copyright 2008 Rémi Audebert <quaero.galileo@gmail.com>
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

# TODO: Un check des messages d'erreur
# TODO: Un update lock sur master pour eviter de commence à travailler sur qqch d'obsolete.
import os, sys, time
from ConfigParser import SafeConfigParser
from subprocess import Popen, PIPE, STDOUT

import wx, threading, Queue, sys, time
from wx.lib.newevent import NewEvent
import  wx.lib.dialogs

# Some constant
VERSION = "0.2"
DEFAULTCONFIG = """
[General]
init = False
lock_user = False
user = 0

"""

wxStdOut, EVT_STDDOUT= NewEvent()
wxWorkerDone, EVT_WORKER_DONE= NewEvent()

class User(object):
    """ Default user """
    username = "audebert"
    branch = "master"
    #~ password = "thisisafuckinpassword" # En fait on en a pas besoin grace à ssh \o/
    name = "Dieu le tout puissant"
    email = "quaero.galileo@gmail.com"

class Bercelli(User):
    username = "bercelli"
    branch = "lorenzo"
    name = "Lorenzo Bercelli"
    email = "colonel_machoute@hotmail.fr"

class Magois(User):
    username = "magois"
    branch = "jules"
    name = "Jules Magois"
    email = "crazyjules@hotmail.fr"

class Audebert(User):
    username = "audebert"
    branch = "remi"
    name = u"Rémi Audebert"
    email = "quaero.galileo@gmail.com"

user = User()

def run_command(command):
    p = Popen(command, shell=True, stdout=PIPE, stderr=STDOUT)
    while not p.poll() and not p.poll() == 0:
        out =  p.stdout.read()
        if out != "\n" and out != "":
            print out

class MainFrame(wx.Frame):
    def __init__(self, *args, **kwds):
        kwds["style"] = wx.DEFAULT_FRAME_STYLE

        self.requestQ = Queue.Queue() #create queues
        self.resultQ = Queue.Queue()

        wx.Frame.__init__(self, *args, **kwds)

        # Config
        self.config = SafeConfigParser()
        self.config.read('lwt.ini')

        # Staticthings
        self.label_1 = wx.StaticText(self, -1, "Auteur")
        self.label_3 = wx.StaticText(self, -1, "Texte expliquant les modifications")
        self.console_label = wx.StaticText(self, -1, u"Console")
        self.static_line_1 = wx.StaticLine(self, -1)


        # Widgets
        self.authors_list = wx.ComboBox(self, -1, choices=["Jules Magois", "Lorenzo Bercelli", u"Rémi Audebert"], style=wx.CB_DROPDOWN|wx.CB_READONLY)
        self.lock_author = wx.CheckBox(self, -1, "Bloquer sur l'utilisateur")
        self.see_pdf_b = wx.Button(self, -1, u"Voir le bouquin")
        self.update_b = wx.Button(self, -1, u"Mettre à jour")
        self.commit_b = wx.Button(self, -1, "Enregistrer les modifications")
        self.merge_b = wx.Button(self, -1, "Envoyer mes modififactions")
        self.diff_b = wx.Button(self, -1, "Voir mes modifications")
        self.diff_l_b = wx.Button(self, -1, "Voir mes modifications locales")
        self.console_output = wx.TextCtrl(self, -1, "", style=wx.TE_MULTILINE|wx.HSCROLL|wx.TE_DONTWRAP)
        self.commit_msg_txt = wx.TextCtrl(self, -1, "", style=wx.TE_MULTILINE)
        self.console_output_timer = wx.Timer(self.console_output, -1)

        # Events
        self.Bind(wx.EVT_COMBOBOX, self.change_user_cb, self.authors_list)
        self.Bind(wx.EVT_CHECKBOX, self.lock_user, self.lock_author)
        self.Bind(wx.EVT_BUTTON, self.update_branch_cb, self.update_b)
        self.Bind(wx.EVT_BUTTON, self.see_pdf, self.see_pdf_b)
        self.Bind(wx.EVT_BUTTON, self.do_commit, self.commit_b)
        self.Bind(wx.EVT_BUTTON, self.merge_master, self.merge_b)
        self.Bind(wx.EVT_BUTTON, self.branch_master_diff, self.diff_b)
        self.Bind(wx.EVT_BUTTON, self.branch_origin_diff, self.diff_l_b)

        self.console_output.Bind(EVT_STDDOUT, self.OnUpdateOutputWindow)
        self.console_output.Bind(wx.EVT_TIMER, self.OnProcessPendingOutputWindowEvents)
        self.Bind(EVT_WORKER_DONE, self.OnWorkerDone)

        #thread
        self.worker = Worker(self, self.requestQ, self.resultQ)

        self.__set_properties()
        self.__do_layout()

        inited = False
        try:
            inited = self.config.get("General", "init")
        except:
            print "Création du fichier de config"
            self.create_config()
        if not inited:
            print "-*- Initialisation -*-"
            self.init_all()
            print "-*- Initialisation terminée -*-"
        self.setup()
        self.update_branch()

    def __del__(self):
        self.config.write(open("lwt.ini", "w"))

    def __set_properties(self):
        self.SetTitle("Lets Write Together, JLR flavor (%s)" % VERSION)
        self.console_output.SetFont(wx.Font(10, wx.DEFAULT, wx.NORMAL, wx.NORMAL, 0, "Monospace"))
        self.console_output.SetMinSize((200, 200))

    def __do_layout(self):
        sizer_2 = wx.BoxSizer(wx.VERTICAL)
        sizer_4 = wx.BoxSizer(wx.VERTICAL)
        sizer_5 = wx.BoxSizer(wx.VERTICAL)
        #~ sizer_5 = wx.StaticBoxSizer(self.sizer_5_staticbox, wx.HORIZONTAL)
        sizer_7 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_6 = wx.BoxSizer(wx.VERTICAL)
        sizer_3 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_3.Add(self.label_1, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL, 10)
        sizer_3.Add(self.authors_list, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL, 0)
        sizer_3.Add(self.lock_author, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL|wx.LEFT, 4)
        sizer_2.Add(sizer_3, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL, 0)
        sizer_5.Add(self.update_b, 0, wx.TOP|wx.BOTTOM|wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL, 4)
        sizer_5.Add(self.see_pdf_b, 0, wx.TOP|wx.BOTTOM|wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL, 4)
        sizer_6.Add(self.label_3, 0, wx.BOTTOM|wx.ALIGN_CENTER_HORIZONTAL, 4)
        sizer_6.Add(self.commit_msg_txt, 0, wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL, 0)
        sizer_5.Add(sizer_6, 1, wx.EXPAND, 0)
        sizer_5.Add(self.commit_b, 0, wx.TOP|wx.BOTTOM|wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL, 4)
        sizer_5.Add(self.static_line_1, 0, wx.TOP|wx.BOTTOM|wx.EXPAND, 4)
        sizer_7.Add(self.merge_b, 0, wx.LEFT|wx.RIGHT|wx.ALIGN_CENTER_HORIZONTAL, 4)
        sizer_7.Add(self.diff_b, 0, wx.LEFT|wx.RIGHT, 4)
        sizer_7.Add(self.diff_l_b, 0, wx.LEFT|wx.RIGHT, 4)
        sizer_5.Add(sizer_7, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL, 0)
        sizer_2.Add(sizer_5, 1, wx.EXPAND, 0)
        sizer_4.Add(self.console_label, 0, wx.TOP|wx.BOTTOM|wx.ALIGN_CENTER_HORIZONTAL, 5)
        sizer_4.Add(self.console_output, 0, wx.EXPAND, 0)
        sizer_2.Add(sizer_4, 0, wx.EXPAND, 0)
        self.SetSizer(sizer_2)
        sizer_2.Fit(self)
        sizer_2.SetSizeHints(self)
        self.Layout()
        self.Centre()

    def OnUpdateOutputWindow(self, event):
        value = event.text
        self.console_output.AppendText(value)

    def OnWorkerDone(self, event):
        self.console_output_timer.Stop()

    def OnProcessPendingOutputWindowEvents(self, event):
        self.console_output.ProcessPendingEvents()

    # -**- Core -**-

    def command_to_console(self, command):
        if sys.platform == 'win32':
            command = command.encode('iso-8859-15')
        self.console_output_timer.Start(50)
        self.worker.begin(run_command, command)

    def create_config(self):
        c_file = open("lwt.ini", "w")
        c_file.write(DEFAULTCONFIG)
        c_file.close()
        self.config.read("lwt.ini")

    def init_all(self):
        global user
        self.command_to_console("git config --global core.autocrlf true")
        if sys.platform == 'win32':
            self.command_to_console("git config --global i81n.commitEncoding UTF8")
        self.command_to_console("git clone git@github.com:audebert/lwt.git")
        time.sleep(5)
        os.chdir("lwt")
        self.command_to_console('git checkout -b remi origin/remi --track')
        self.command_to_console('git checkout -b jules origin/jules --track')
        self.command_to_console('git checkout -b lorenzo origin/lorenzo --track')
        self.change_user('Master')

    def set_console_label(self, label):
        self.console_label.SetLabel("Console : %s" % label)

    def setup(self):
        global user
        if self.config.get("General", "user") != '0':
            self.authors_list.SetValue(self.config.get("General", "user"))

        if self.config.getboolean("General", "lock_user") is True:
            self.authors_list.Disable()
            self.lock_author.SetValue(True)
        username = self.config.get("General", "user")
        if username == "audebert":
            self.change_user(u"Rémi Audebert")
        elif username == "bercelli":
            self.change_user(u"Lorenzo Bercelli")
        elif username == "magois":
            self.change_user(u"Jules Magois")
        if username != "0":
            self.authors_list.SetValue(user.name)

    def lock_user(self, event):
        if self.authors_list.IsEnabled():
            self.authors_list.Disable()
            self.config.set("General", "lock_user", "True")
        else :
            self.authors_list.Enable()
            self.config.set("General", "lock_user", "False")

    def change_user(self, name):
        global user
        if name == u"Lorenzo Bercelli":
            user = Bercelli()
        elif name == u"Jules Magois":
            user = Magois()
        elif name == u"Rémi Audebert":
            user = Audebert()
        elif name == u"Master":
            user = User()
        self.command_to_console('git config --global user.name "%s"' % user.name)
        self.command_to_console('git config --global user.email %s'% user.email)
        self.change_branch(user)

        self.config.set("General", "user", user.username)

    def change_user_cb(self, event):
        name = event.GetString()
        self.change_user(name)

    def update_branch_cb(self, event):
        self.update_branch()

    # -**- Git -**-

    def change_branch(self,user):
        self.command_to_console('git checkout %s' % user.branch)

    def update_branch(self):
        self.command_to_console("git pull origin master")
        self.command_to_console("git pull origin %s" % user.branch)

    def do_commit(self, event):
        global user
        self.command_to_console('git add .')
        self.command_to_console('git commit -m "%s"' % self.commit_msg_txt.GetValue().replace('"',"'"))
        self.command_to_console('git push origin %s' % user.branch)

    def merge_master(self, event):
        global user
        old_user = user
        self.change_user('Master')
        self.command_to_console('git merge %s' % old_user.branch)
        self.command_to_console('git push origin')
        self.change_user(old_user.name)

    def branch_master_diff(self, event):
        p = Popen("git diff origin/master", shell=True, stdout=PIPE, stderr=STDOUT)
        out = p.stdout
        p.wait()
        msg = out.read()
        out.close()

        dlg = wx.lib.dialogs.ScrolledMessageDialog(self, msg, "Differences")
        dlg.ShowModal()

    def branch_origin_diff(self, event):
        p = Popen("git diff ", shell=True, stdout=PIPE, stderr=STDOUT)
        out = p.stdout
        p.wait()
        msg = out.read()
        out.close()

        dlg = wx.lib.dialogs.ScrolledMessageDialog(self, msg, "Differences locales")
        dlg.ShowModal()

    # -**- Other -**-
    def see_pdf(self, event):
        os.chdir(r'latex')
        self.command_to_console('pdflatex --file-line-error-style -interaction nonstopmode main.tex')
        if sys.platform == 'linux2':
            os.system('evince main.pdf')
        elif sys.platform == 'win32':
            os.system('"C:\Program Files\Adobe\Reader 9.0\Reader\AcroRd32.exe" main.pdf')
        elif sys.platform == 'mac':
            pass
        os.chdir(r'..')

class Worker(threading.Thread):
    requestID = 0
    def __init__(self, parent, requestQ, resultQ, **kwds):
        threading.Thread.__init__(self, **kwds)
        self.setDaemon(True)
        self.requestQ = requestQ
        self.resultQ = resultQ
        self.parent = parent
        self.start()

    def begin(self, callable, *args, **kwds):
        Worker.requestID +=1
        self.requestQ.put((Worker.requestID, callable, args, kwds))
        return Worker.requestID

    def run(self):
        while True:
            requestID, callable, args, kwds = self.requestQ.get()
            self.resultQ.put((requestID, callable(*args, **kwds)))
            evt = wxWorkerDone()
            wx.PostEvent(self.parent, evt)

class SysOutListener:
    def fileno(self, *args, **kwargs):
        return sys.__stdout__.fileno()

    def write(self, string):
        sys.__stdout__.write(string)
        evt = wxStdOut(text=string)
        wx.PostEvent(wx.GetApp().frame.console_output, evt)

class App(wx.App):
    def OnInit(self):
       self.frame = MainFrame(None, -1, '')
       self.frame.Show(True)
       self.frame.Center()
       return True


if __name__ == "__main__":
    App = App()
    sys.stdout = SysOutListener()
    App.MainLoop()
